final BASE_URL = 'http://192.168.40.113:8080/api';
final CREATE_USER = '/users/create';
final LIST_USERS = '/users';
final GET_USER = '/users/';
final DELETE_USER = '/users/delete';
final EDIT_USER = 'users/edit';
