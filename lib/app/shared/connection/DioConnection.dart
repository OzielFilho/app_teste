import 'package:dio/dio.dart';
import 'package:pratico/app/modules/home/models/User.dart';
import 'package:pratico/app/shared/connection/IDioConnection.dart';
import 'package:pratico/app/shared/utils/strings_helpers.dart';

class DioConnection implements IDioConnection {
  late Dio client;

  DioConnection() {
    BaseOptions options = BaseOptions(
      baseUrl: "${BASE_URL}",
      connectTimeout: 5000,
    );
    client = Dio(options);
  }

  @override
  requestPost({required String url, required Map<String, dynamic> data}) async {
    Response response = await client.post('${url}', data: data);
    return response.data;
  }

  @override
  Future<dynamic> requestGet({required String url}) async {
    Response response = await client.get('${url}');
    return response.data;
  }

  @override
  requestDelete({required String url, required int id}) async {
    Response response = await client.delete('${url}', data: id);
    return response.data;
  }

  @override
  requestEdit({required String url, required Map<String, dynamic> data}) async {
    Response response = await client.put('${url}', data: data);
    return response.data;
  }
}
