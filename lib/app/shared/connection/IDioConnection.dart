import 'package:pratico/app/modules/home/models/User.dart';

abstract class IDioConnection {
  requestPost({required String url, required Map<String, dynamic> data});
  Future<dynamic> requestGet({required String url});
  requestDelete({required String url, required int id});
  requestEdit({required String url, required Map<String, dynamic> data});
}
