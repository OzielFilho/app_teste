import 'package:pratico/app/modules/home/models/User.dart';
import 'package:pratico/app/modules/home/repositories/home_repository_interface.dart';
import 'package:pratico/app/shared/connection/DioConnection.dart';
import 'package:pratico/app/shared/utils/strings_helpers.dart';

class HomeConnectionRepository implements IHomeRepository {
  late final DioConnection connection;

  HomeConnectionRepository(this.connection);

  @override
  Future<User> create(User model) async {
    return await connection.requestPost(url: CREATE_USER, data: model.toJson());
  }

  @override
  delete(int id) {
    return connection.requestDelete(
        url: DELETE_USER + '/' + id.toString(), id: id);
  }

  @override
  edit(User model) {
    return connection.requestEdit(url: EDIT_USER, data: model.toJson());
  }

  @override
  Future<List<User>> getUsers() async {
    List<User> users = [];
    await connection.requestGet(url: LIST_USERS).then((value) {
      value.forEach((user) {
        users.add(User.fromJson(user));
      });
    });
    return users;
  }

  @override
  Future<User> getUser(String id) async {
    User user = User();
    await connection.requestGet(url: GET_USER + id).then((value) {
      user = User.fromJson(value);
    });
    return user;
  }
}
