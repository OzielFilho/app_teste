import 'package:pratico/app/modules/home/models/User.dart';

abstract class IHomeRepository {
  Future<List<User>> getUsers();

  Future<User> getUser(String id);

  Future<User> create(User model);

  edit(User model);

  delete(int id);
}
