import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class TextFormFieldEdit extends StatefulWidget {
  final TextEditingController? textEditingController;
  final String? title;
  final bool? isBirth;
  const TextFormFieldEdit(
      {Key? key, this.textEditingController, this.title, this.isBirth})
      : super(key: key);

  @override
  _TextFormFieldEditState createState() => _TextFormFieldEditState();
}

class _TextFormFieldEditState extends State<TextFormFieldEdit> {
  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: EdgeInsetsDirectional.fromSTEB(20, 0, 20, 0),
          child: Text(
            '${widget.title}',
            style: TextStyle(color: Colors.white),
          ),
        ),
        const SizedBox(
          height: 10,
        ),
        Padding(
          padding: EdgeInsetsDirectional.fromSTEB(20, 0, 20, 20),
          child: Container(
            width: double.infinity,
            height: 50,
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(25),
            ),
            child: Padding(
              padding: EdgeInsetsDirectional.fromSTEB(12, 0, 20, 0),
              child: TextFormField(
                controller: widget.textEditingController,
                obscureText: false,
                decoration: InputDecoration(
                  labelText: widget.title,
                  hintText: widget.title,
                  hintStyle: TextStyle(
                    color: Color(0xff1e2070),
                  ),
                  enabledBorder: OutlineInputBorder(
                    borderSide: BorderSide(
                      color: Color(0x00000000),
                      width: 1,
                    ),
                    borderRadius: const BorderRadius.only(
                      topLeft: Radius.circular(4.0),
                      topRight: Radius.circular(4.0),
                    ),
                  ),
                  focusedBorder: OutlineInputBorder(
                    borderSide: BorderSide(
                      color: Color(0x00000000),
                      width: 1,
                    ),
                    borderRadius: const BorderRadius.only(
                      topLeft: Radius.circular(4.0),
                      topRight: Radius.circular(4.0),
                    ),
                  ),
                ),
                style: TextStyle(
                  color: Color(0xff1e2070),
                ),
                keyboardType: widget.isBirth ?? false
                    ? TextInputType.number
                    : TextInputType.text,
              ),
            ),
          ),
        ),
      ],
    );
  }
}
