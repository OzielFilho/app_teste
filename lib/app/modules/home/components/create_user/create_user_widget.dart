import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:pratico/app/modules/home/components/buttom_edit/buttom_edit.dart';
import 'package:pratico/app/modules/home/components/modals/modal_bottom_edit/modal_bottom_edit_widget.dart';
import 'package:pratico/app/modules/home/components/textformfield_edit/textformfield_edit.dart';
import 'package:pratico/app/modules/home/home_store.dart';
import 'package:pratico/app/modules/home/models/User.dart';

class CreateUser extends StatefulWidget {
  final Function? state;
  const CreateUser({Key? key, this.state}) : super(key: key);

  @override
  _CreateUserState createState() => _CreateUserState();
}

class _CreateUserState extends State<CreateUser> {
  late User user;

  TextEditingController _nameController = TextEditingController();
  TextEditingController _address = TextEditingController();
  TextEditingController _birth = TextEditingController();
  String _valueSex = 'O';
  final homeStore = Modular.get<HomeStore>();

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          Text(
            'Create User',
            style: TextStyle(
                color: Colors.white, fontSize: 15, fontWeight: FontWeight.bold),
          ),
          const SizedBox(
            height: 15,
          ),
          CircleAvatar(
            backgroundColor: Colors.white,
            radius: 60,
            backgroundImage: MemoryImage(base64Decode(homeStore.img64)),
          ),
          const SizedBox(
            height: 15,
          ),
          CircleAvatar(
              radius: 15,
              backgroundColor: Colors.white,
              child: IconButton(
                  iconSize: 15,
                  onPressed: () {
                    ModalBottomEditPhoto(context, widget.state ?? () {});
                  },
                  icon: Icon(Icons.camera_alt))),
          const SizedBox(
            height: 15,
          ),
          TextFormFieldEdit(
            isBirth: false,
            textEditingController: _nameController,
            title: 'Name',
          ),
          TextFormFieldEdit(
            isBirth: false,
            textEditingController: _address,
            title: 'Address',
          ),
          TextFormFieldEdit(
            isBirth: false,
            textEditingController: _birth,
            title: 'Birth',
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                'Gender',
                style: TextStyle(
                    color: Colors.white,
                    fontSize: 15,
                    fontWeight: FontWeight.bold),
              ),
              const SizedBox(
                width: 15,
              ),
              Row(
                children: [
                  Text(
                    'M',
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 15,
                        fontWeight: FontWeight.w500),
                  ),
                  Radio(
                    value: 'M',
                    groupValue: _valueSex,
                    onChanged: (value) {
                      setState(() {
                        _valueSex = 'M';
                      });
                    },
                  ),
                ],
              ),
              Row(
                children: [
                  Text(
                    'W',
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 15,
                        fontWeight: FontWeight.w500),
                  ),
                  Radio(
                    value: 'W',
                    groupValue: _valueSex,
                    onChanged: (value) {
                      setState(() {
                        _valueSex = 'W';
                      });
                    },
                  ),
                ],
              ),
              Row(
                children: [
                  Text(
                    'Other',
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 15,
                        fontWeight: FontWeight.w500),
                  ),
                  Radio(
                      value: 'O',
                      groupValue: _valueSex,
                      onChanged: (value) {
                        setState(() {
                          _valueSex = 'O';
                        });
                      }),
                ],
              ),
            ],
          ),
          const SizedBox(
            height: 15,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              ButtomEdit(
                function: () {
                  User user = User(
                      address: _address.text,
                      birth: _birth.text,
                      name: _nameController.text,
                      sex: _valueSex,
                      photo: Modular.get<HomeStore>().img64);
                  homeStore.save(user, widget.state ?? () {});
                },
                state: widget.state,
                title: 'Create',
              ),
              ButtomEdit(
                function: () {},
                state: widget.state,
                title: 'Back',
              ),
            ],
          )
        ],
      ),
    );
  }
}
