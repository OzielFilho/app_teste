import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:image_picker/image_picker.dart';
import 'package:pratico/app/modules/home/components/buttom_edit/buttom_edit.dart';
import 'package:pratico/app/modules/home/home_store.dart';

ModalBottomEditPhoto(
  BuildContext context,
  Function state,
) {
  Modular.get<HomeStore>().showCreate = true;
  state();
  return showModalBottomSheet(
    context: context,
    shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
            topLeft: Radius.circular(15), topRight: Radius.circular(15))),
    isScrollControlled: false,
    builder: (context) {
      return Container(
        height: 100,
        decoration: BoxDecoration(
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(15), topRight: Radius.circular(15))),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            Text('Choose One Option'),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                ButtomEdit(
                  title: 'Cam',
                  state: state,
                  function: () {
                    Modular.get<HomeStore>().getImage(ImageSource.camera);
                    Modular.get<HomeStore>().showCreate = true;
                    state();
                  },
                ),
                ButtomEdit(
                  state: state,
                  title: 'Gallery',
                  function: () {
                    Modular.get<HomeStore>().getImage(ImageSource.gallery);
                    Modular.get<HomeStore>().showCreate = true;
                    state();
                  },
                )
              ],
            ),
          ],
        ),
      );
    },
  );
}
