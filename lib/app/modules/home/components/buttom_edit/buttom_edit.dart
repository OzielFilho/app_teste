import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:pratico/app/modules/home/home_store.dart';

class ButtomEdit extends StatefulWidget {
  final String? title;
  final Function? state;
  final Function? function;
  const ButtomEdit({Key? key, this.title, this.state, this.function})
      : super(key: key);

  @override
  _ButtomEditState createState() => _ButtomEditState();
}

class _ButtomEditState extends State<ButtomEdit> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Center(
        child: MaterialButton(
          color: Colors.white,
          onPressed: () {
            widget.function!();
            Modular.get<HomeStore>().showCreate =
                !Modular.get<HomeStore>().showCreate;

            widget.state!();
          },
          child: Text(
            widget.title ?? '',
            style: TextStyle(color: Color(0xff1e2070)),
          ),
        ),
      ),
    );
  }
}
