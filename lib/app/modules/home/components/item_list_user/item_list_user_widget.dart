import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:pratico/app/modules/home/home_store.dart';
import 'package:pratico/app/modules/home/models/User.dart';

class ItemListUser extends StatefulWidget {
  final User? user;
  final Function? state;
  final int? index;
  const ItemListUser({Key? key, this.user, this.index, this.state})
      : super(key: key);

  @override
  _ItemListUserState createState() => _ItemListUserState();
}

class _ItemListUserState extends State<ItemListUser> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        ListTile(
          title: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              CircleAvatar(
                backgroundColor: Colors.white,
                backgroundImage:
                    MemoryImage(base64Decode(widget.user?.photo ?? '')),
              ),
              const SizedBox(
                width: 10,
              ),
              Text(
                widget.user?.name ?? '',
                style: TextStyle(color: Colors.white),
              ),
            ],
          ),
          subtitle: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const SizedBox(
                height: 10,
              ),
              Text(
                'Address: ${widget.user?.address ?? ''}',
                style: TextStyle(color: Colors.white),
              ),
              Text(
                'Sex: ${widget.user?.sex ?? ''}',
                style: TextStyle(color: Colors.white),
              ),
              Text(
                'Birth: ${widget.user?.birth ?? ''}',
                style: TextStyle(color: Colors.white),
              ),
            ],
          ),
          trailing: IconButton(
            icon: Icon(
              Icons.remove_circle_outline,
              color: Colors.red,
            ),
            onPressed: () {
              Modular.get<HomeStore>().delete(widget.user?.id ?? 0,
                  widget.index ?? 0, widget.state ?? () {});
            },
          ),
        ),
        Divider(
          color: Colors.white,
        )
      ],
    );
  }
}
