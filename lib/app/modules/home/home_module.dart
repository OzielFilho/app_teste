import 'package:flutter_modular/flutter_modular.dart';
import 'package:pratico/app/modules/home/repositories/home_connection_repository.dart';
import 'package:pratico/app/modules/home/repositories/home_repository_interface.dart';
import 'package:pratico/app/shared/connection/DioConnection.dart';
import '../home/home_store.dart';

import 'home_page.dart';

class HomeModule extends Module {
  @override
  final List<Bind> binds = [
    Bind.lazySingleton((i) => HomeStore(i.get())),
    Bind<IHomeRepository>((i) => HomeConnectionRepository(i.get())),
    Bind((i) => DioConnection())
  ];

  @override
  final List<ModularRoute> routes = [
    ChildRoute(Modular.initialRoute, child: (_, args) => HomePage()),
  ];
}
