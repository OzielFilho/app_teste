class User {
  int? id;
  String? name;
  String? photo;
  String? birth;
  String? address;
  String? sex;

  User({this.name, this.id, this.photo, this.birth, this.address, this.sex});

  User.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    photo = json['photo'];
    birth = json['birth'];
    address = json['address'];
    sex = json['sex'];
    id = json['id'];
  }

  Map<String, dynamic> toJson() {
    Map<String, dynamic> json = new Map<String, dynamic>();
    json['name'] = this.name;
    json['photo'] = this.photo;
    json['birth'] = this.birth;
    json['address'] = this.address;
    json['sex'] = this.sex;
    json['id'] = this.id;
    return json;
  }
}
