import 'dart:convert';

import 'package:image_picker/image_picker.dart';
import 'package:mobx/mobx.dart';
import 'package:pratico/app/modules/home/models/User.dart';
import 'package:pratico/app/modules/home/repositories/home_repository_interface.dart';

part 'home_store.g.dart';

class HomeStore = _HomeStoreBase with _$HomeStore;

abstract class _HomeStoreBase with Store {
  late final IHomeRepository repository;

  _HomeStoreBase(this.repository) {
    getUsers();
  }

  @observable
  bool showCreate = false;

  @observable
  List<User> userList = [];

  @observable
  String img64 = '';

  ImagePicker _picker = ImagePicker();
  var _image;

  @action
  getImage(
    ImageSource source,
  ) async {
    showCreate = true;
    var imageFilePath = await _picker.pickImage(source: source);
    final bytes = imageFilePath!.readAsBytes();
    await bytes.then((value) {
      img64 = base64Encode(value);
    });
    showCreate = true;
  }

  @action
  getUsers() {
    userList.clear();
    repository.getUsers().then((value) {
      userList = value;
    });
  }

  @action
  save(User model, Function state) async {
    await repository.create(model).whenComplete(() {
      getUsers();
      state();
    });
  }

  @action
  void delete(int id, int index, Function state) {
    repository.delete(id);
    userList.removeAt(index);
    getUsers();
    state();
  }

  @observable
  int counter = 0;

  Future<void> increment() async {
    counter = counter + 1;
  }
}
