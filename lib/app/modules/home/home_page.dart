import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:pratico/app/modules/home/components/buttom_edit/buttom_edit.dart';
import 'package:pratico/app/modules/home/components/create_user/create_user_widget.dart';
import 'package:pratico/app/modules/home/components/item_list_user/item_list_user_widget.dart';

import 'home_store.dart';
import 'models/User.dart';

class HomePage extends StatefulWidget {
  final String title;
  const HomePage({Key? key, this.title = "Home"}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends ModularState<HomePage, HomeStore> {
  statePage() {
    setState(() {});
  }

  @override
  void initState() {
    // TODO: implement initState
    controller.getUsers();
    super.initState();
  }

  ScrollController _controller = ScrollController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xff1e2070),
      body: Observer(builder: (context) {
        return SafeArea(
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: SingleChildScrollView(
              controller: _controller,
              child: Column(
                children: [
                  Text(
                    'Users',
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 15,
                        fontWeight: FontWeight.bold),
                  ),
                  Divider(
                    color: Colors.white,
                  ),
                  const SizedBox(
                    height: 15,
                  ),
                  controller.userList.isNotEmpty
                      ? ListView.builder(
                          controller: _controller,
                          shrinkWrap: true,
                          itemCount: controller.userList.length,
                          itemBuilder: (context, index) => ItemListUser(
                            user: controller.userList[index],
                            state: statePage,
                          ),
                        )
                      : Container(
                          child: Text(
                            'Users not found!',
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 15,
                                fontWeight: FontWeight.w400),
                          ),
                        ),
                  const SizedBox(
                    height: 15,
                  ),
                  Divider(
                    color: Colors.white,
                  ),
                  controller.showCreate
                      ? CreateUser(
                          state: statePage,
                        )
                      : ButtomEdit(
                          title: 'Create User',
                          state: statePage,
                          function: () {},
                        )
                ],
              ),
            ),
          ),
        );
      }),
    );
  }
}
