// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'home_store.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$HomeStore on _HomeStoreBase, Store {
  final _$showCreateAtom = Atom(name: '_HomeStoreBase.showCreate');

  @override
  bool get showCreate {
    _$showCreateAtom.reportRead();
    return super.showCreate;
  }

  @override
  set showCreate(bool value) {
    _$showCreateAtom.reportWrite(value, super.showCreate, () {
      super.showCreate = value;
    });
  }

  final _$userListAtom = Atom(name: '_HomeStoreBase.userList');

  @override
  List<User> get userList {
    _$userListAtom.reportRead();
    return super.userList;
  }

  @override
  set userList(List<User> value) {
    _$userListAtom.reportWrite(value, super.userList, () {
      super.userList = value;
    });
  }

  final _$img64Atom = Atom(name: '_HomeStoreBase.img64');

  @override
  String get img64 {
    _$img64Atom.reportRead();
    return super.img64;
  }

  @override
  set img64(String value) {
    _$img64Atom.reportWrite(value, super.img64, () {
      super.img64 = value;
    });
  }

  final _$counterAtom = Atom(name: '_HomeStoreBase.counter');

  @override
  int get counter {
    _$counterAtom.reportRead();
    return super.counter;
  }

  @override
  set counter(int value) {
    _$counterAtom.reportWrite(value, super.counter, () {
      super.counter = value;
    });
  }

  final _$getImageAsyncAction = AsyncAction('_HomeStoreBase.getImage');

  @override
  Future getImage(ImageSource source) {
    return _$getImageAsyncAction.run(() => super.getImage(source));
  }

  final _$saveAsyncAction = AsyncAction('_HomeStoreBase.save');

  @override
  Future save(User model, Function state) {
    return _$saveAsyncAction.run(() => super.save(model, state));
  }

  final _$_HomeStoreBaseActionController =
      ActionController(name: '_HomeStoreBase');

  @override
  dynamic getUsers() {
    final _$actionInfo = _$_HomeStoreBaseActionController.startAction(
        name: '_HomeStoreBase.getUsers');
    try {
      return super.getUsers();
    } finally {
      _$_HomeStoreBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  void delete(int id, int index, Function state) {
    final _$actionInfo = _$_HomeStoreBaseActionController.startAction(
        name: '_HomeStoreBase.delete');
    try {
      return super.delete(id, index, state);
    } finally {
      _$_HomeStoreBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''
showCreate: ${showCreate},
userList: ${userList},
img64: ${img64},
counter: ${counter}
    ''';
  }
}
